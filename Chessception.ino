#include "SmartKit.h"
#include <string.h>
/*******************************************************************
** Chessception Global Parameters                                 **
********************************************************************/
enum games {
  CHESS,
  TICTACTOE
};
// height of game pieces
const int pieceHeight = 40.0000;


enum games game = CHESS;

// theta is rotation angle between our 8x8 grid and dobot coordinate system
const float theta = 3.54773077;
const float tileSize = 24;
const float cosOfTheta = (float)cos(theta);
const float sinOfTheta = (float)sin(theta);

//Real-life location of (0,0)
float xOffset;
float yOffset;
float zOffset;
float rOffset;

//Current position
float current[4];


/*********************************************************************************************************
** Function name:       setupArm
** Descriptions:        Initializes real-life location of (0,0)
** Input parameters:    none
** Output parameters:   none
** Returned value:      none
*********************************************************************************************************/

void setupArm()
{
  static Pose pose;
  GetPose(&pose);
  xOffset = pose.x;
  current[0] = xOffset;
  yOffset = pose.y;
  current[1] = yOffset;
  zOffset = pose.z;
  current[2] = zOffset;
  rOffset = pose.rHead;
  current[3] = rOffset;
}


/********************************************************************************************************************************
** Function name:       moveOnGrid
** Descriptions:        converts game grid coordinates to real-life dobot coordinates and then moves the arm to this position
** Input parameters:    tileX, tileY, numberOfpiecesHigher (how much should the arm raise up relative to piece height)
** Output parameters:   none
** Returned value:      none
********************************************************************************************************************************/


void moveOnGrid(float tileX, float tileY, float numberOfPiecesHigher){
  // determine dx, dy and dz
  float dy = tileY * tileSize;
  float dx = tileX * tileSize;
  float dz = zOffset - tileY * 0.3;

  //Do all the necessary calculations with sin and cos for the transformation formula
  float xTimesCos = dx*cosOfTheta;
  float yTimesCos = dy*cosOfTheta;
  float xTimesSin = dx*sinOfTheta;
  float yTimesSin = dy*sinOfTheta;
  float heightAbovePiece = numberOfPiecesHigher * pieceHeight;

  //Determine destinations coordinates for x and y with the transformation formula
  float dobotX = xTimesCos + yTimesSin + xOffset;
  float dobotY = - xTimesSin + yTimesCos + yOffset;
  
  //This pattern for R was deducted from dobot studio, but probably unnecessary
  float dobotR = tileY*-2 + tileX*-4 + rOffset;

  current[0] = dobotX;
  current[1] = dobotY;
  current[2] = dz + heightAbovePiece;
  current[3] = dobotR;

  Dobot_SetPTPCmd(MOVJ_XYZ, dobotX, dobotY, dz + heightAbovePiece, dobotR);

  delay(1000);
}


/*********************************************************************************************************
** Function name:       goToRest
** Descriptions:        Moves arm back to rest position above board
** Input parameters:    none
** Output parameters:   none
** Returned value:      none
*********************************************************************************************************/

void goToRest(){
  Dobot_SetPTPCmd(MOVJ_XYZ, current[0], current[1], current[2]+(1.5*pieceHeight), current[3]);
  if(game == CHESS)
    moveOnGrid(2.5,5.5,1.5);
  else
    moveOnGrid(1.0,3.0,1.5);
}






/*********************************************************************************************************
** Function name:       movePiece
** Descriptions:        Picks up a piece at its current location and moves it to its new destination
** Input parameters:    xOld, yOld, xNew, yNew
** Output parameters:   none
** Returned value:      newY
*********************************************************************************************************/

void movePiece(float xOld, float yOld, float xNew, float yNew)
{     
      // hover above piece to be moved
      moveOnGrid(xOld, yOld, 1.25);
      // lower onto piece head
      moveOnGrid(xOld, yOld, 0);
      // activate suction cup
      Dobot_SetEndEffectorSuctionCup(true);

      // lift piece
      moveOnGrid(xOld, yOld, 1.25);

      // hover above destination
      moveOnGrid(xNew, yNew, 1.25);

      // lower piece onto destination
      moveOnGrid(xNew, yNew, 0);

      //deactivate suction cup
      Dobot_SetEndEffectorSuctionCup(false);
      
      // Go to rest
      goToRest();
}

char inputBuffer[40];
char startSign = '<';
char endSign = '>';
static byte bytesRecvd = 0;

int count = 3;
int rdy = 1;

/*****************************************************************************************************************
** Function name:       readSerial
** Descriptions:        Read incoming serial input between start and end sign (e.g. asdls<hallo>askl reads hallo) 
** Input parameters:    none
** Output parameters:   none
** Returned value:      none
******************************************************************************************************************/



void readSerial(){
  char x = 'a';

  // Look for the start sign
  while(x != startSign){
    if(Serial.available()){
      x = (char)Serial.read();
    }
  }

  // Put all bytes in the inputbuffer until the endsign is found
  while(x != endSign){
    if(Serial.available()){
      x = Serial.read();
      if( x != endSign){
        inputBuffer[bytesRecvd] = x;
        bytesRecvd++;
      } else {
        // terminate string and set bytesRecvd back to zero
        inputBuffer[bytesRecvd] = '\0';
        bytesRecvd = 0;
      }
    }
  }
  
}


void setup() {
    Serial.begin(115200);
    Dobot_Init();
}

void loop() {
    if(count == 3){
      if(rdy == 1){
        Serial.println("Ready");
        rdy--;
      }
      if(Serial.available())
        count--;
    }
    else if(count == 2){
      // initialize the game
      
      readSerial();
      int gameID = atoi(inputBuffer);
      
      switch(gameID){
        case 0:
           game = CHESS;
           break;
        case 1:
           game = TICTACTOE;
           break;
      }
      count--;
    }
    else if(count == 1){
      setupArm();
      if(game == CHESS)
        movePiece(0,0,0,2);
      goToRest();
      count--;
    }
    else if (count == 0){
      int numberOfMoves;
      float coordinates[4];
      if(Serial.available()){
        readSerial();

        // We will initially either receive the message GAME OVER or a number that is equal to the amount of moves that need to be processed
        if ( strcmp(inputBuffer, "GAME OVER") == 0){
          count = 3;
          rdy = 1;
          if(game == TICTACTOE) // Only tictactoe cleans up the fields, chess pieces need to be reset by hand currently
            readSerial();
        }
                 
          numberOfMoves = atoi(inputBuffer);
          Serial.println(String(numberOfMoves));
          while ( numberOfMoves > 0 ){
            for(int i = 0; i < 4; i++){
              readSerial();
              
              coordinates[i] = atof(inputBuffer);
            }

          // Context: the two left and right most columns on a 12x12 square are outside the range of motion of the Dobot
            if (game == CHESS){
              if(coordinates[1] > 1 && coordinates[1] < 10) // if the coordinate is on the playfield
                coordinates[0] -= 1;

              if(coordinates[3] > 1 && coordinates[3] < 10) // if the coordinate is on the playfield
                coordinates[2] -= 1;
            }
            
            // Make the move
            movePiece(coordinates[0],coordinates[1],coordinates[2], coordinates[3]);
            
            numberOfMoves--;
          }
    
          // Let rpi know that dobot made all the moves
          Serial.println("Finished");
      }
    }
}

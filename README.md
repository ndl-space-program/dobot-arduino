Connections between Dobot and Arduino:
--------------------------------------
	To connect to Dobot Base "Communication Interface:
	---------------------------------------------------
		Top left corner: 5V
		Bottom left corner: GND
		Next to 5V: TX2 (pin 16)
		Next to GND: RX2 (pin 17)



To show this API works (or use it in general):
------------------------
1. Extract ArduinoKit(EN)V2.1
2. Cut all libraries (Magician, Pixy, Smartkit) and paste it in ../Arduino/libraries
3. Go to ../libraries/SmartKit/MoveBlockDemo and open it with Arduino IDE to run a simple DEMO involving movement and suction.




Notes:
-------
- New move functions etc need to be based on the code in ../libraries/SmartKit/MoveBlockDemo
- See Arduino-Kit-Demo-Description-2.1(1) for all possible options/functions
- (This API is not made by us, its just here for convenience for future use by other groups)
